import sys
import argparse
import pathlib
import importlib.util

def main() -> int:

    # -------------------------------------------------------------------------
    # argument parsing
    script_path = pathlib.Path(__file__).absolute()
    usage = "%(prog)s [options] APUAPP"
    description = script_path.name + f": run apu appplication"
    parser = argparse.ArgumentParser(usage=usage, description=description)
    parser.add_argument(
        "apuapp",
        help="apu application",
        action="store",
        nargs='?',
        type=str,
        default=None, 
        metavar="APUAPP")
    parser.add_argument(
        "-c",
        "--config-path",
        help="path to folder containing the configuration files",
        dest="config_path",
        action="store",
        type=str,
        default="",
        nargs="?")

    args = parser.parse_args()

    if args.apuapp is None:
        return 0

    # module specification
    module_path = script_path.joinpath(
        script_path.parent,
        "apu_modules",
        f"{args.apuapp}.py")
    spec = importlib.util.spec_from_file_location(args.apuapp,module_path)

    # load module
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    # configuration
    if args.config_path == '':
        config_path = script_path.joinpath(
            script_path.parent,
            "apu_config",
            f"{args.apuapp}")
    else:
        config_path = pathlib.Path(args.config_path)

    a = module.Application(config_path=config_path)
    a.run()

    return 0

if __name__ == '__main__':
    sys.exit(main())
