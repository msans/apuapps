import pathlib
import apu.app as app
import apu.unit.test.dev as dev

class Application(app.ChunkedFileProcessingApp):
    def __init__(self,config_path=pathlib.Path):
        super().__init__(config_path=config_path)

        self.io_init()

        self.unit = dev.Unit(config=self.unit_conf, inherit_par={
                'inchan': self.app_conf['input']['nchan'], 
                'onchan': self.app_conf['output']['nchan'],
                'nframes': self.nframes,
                })
