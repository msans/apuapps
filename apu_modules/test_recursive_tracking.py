import pathlib
import apu.app as app
import apu.unit.test.recursive_tracking as rtrack

class Application(app.FullFileProcessingApp):
    def __init__(self,config_path=pathlib.Path):
        super().__init__(config_path=config_path)

        self.io_init()

        self.unit = rtrack.Unit(config=self.unit_conf, inherit_par={
                'inchan': self.app_conf['input']['nchan'], 
                'onchan': self.app_conf['output']['nchan'],
                'nframes': self.nframes,
                })
